Name:           jsoncpp
Version:        1.9.3
Release:        1
Summary:        JSON C++ library
License:        Public Domain or MIT
URL:            https://github.com/open-source-parsers/jsoncpp
Source0:        https://github.com/open-source-parsers/jsoncpp/archive/%{version}/%{name}-%{version}.tar.gz
BuildRequires:  gcc-c++ cmake >= 3.1 python3-devel jsoncpp

%description
JsonCpp is a C++ library that allows manipulating JSON values,
including serialization and deserialization to and from strings.
It can also preserve existing comment in unserialization/serialization steps,
making it a convenient format to store user input files.

%package        devel
Summary:        Development files for jsoncpp
Requires:       %{name} = %{version}-%{release}

%description    devel
Document files contain library and head files for jsoncpp.

%package        help
Summary:        Document for jsoncpp
BuildRequires:  doxygen graphviz hardlink
BuildArch:      noarch
Provides:       jsoncpp-doc = %{version}-%{release}
Obsoletes:      jsoncpp-doc < %{version}-%{release}

%description    help
Help document for jsoncpp.

%prep
%autosetup -p 1
doxygen -s -u doc/doxyfile.in
sed -i -e 's!^DOT_FONTNAME.*=.*!DOT_FONTNAME =!g' doc/doxyfile.in

%build
install -d %{_vpath_builddir}
cd %{_vpath_builddir}
%cmake -DBUILD_STATIC_LIBS=OFF -DJSONCPP_WITH_WARNING_AS_ERROR=OFF \
       -DJSONCPP_WITH_PKGCONFIG_SUPPORT=ON -DJSONCPP_WITH_CMAKE_PACKAGE=ON \
       -DJSONCPP_WITH_POST_BUILD_UNITTEST=OFF -DPYTHON_EXECUTABLE="%{__python3}" \
       ..
cd ..
%make_build -C %{_vpath_builddir}
cp -p %{_vpath_builddir}/version .
%{__python3} doxybuild.py --with-dot --doxygen %{_bindir}/doxygen
rm -f version

%install
%make_install -C %{_vpath_builddir}
mkdir -p %{buildroot}%{_docdir}/%{name}
cp README.md %{buildroot}%{_docdir}/%{name}
chmod 644 %{buildroot}%{_docdir}/%{name}/README.md
touch %{buildroot}%{_docdir}/%{name}//README.md
mkdir -p %{buildroot}%{_docdir}/%{name}/html
cp dist/doxygen/*/*.{html,png} %{buildroot}%{_docdir}/%{name}/html
chmod 644 %{buildroot}%{_docdir}/%{name}/html/*.{html,png}
touch %{buildroot}%{_docdir}/%{name}/html
hardlink -cfv %{buildroot}%{_docdir}/%{name}
cp -p %{_libdir}/libjsoncpp.so.* %{buildroot}%{_libdir}

%check
%make_build -C %{_vpath_builddir} jsoncpp_check

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%license AUTHORS LICENSE
%doc %dir %{_docdir}/%{name}
%exclude %{_docdir}/%{name}/html
%{_libdir}/lib%{name}.so.*
%{_libdir}/lib%{name}.so.%{version}

%files devel
%{_libdir}/lib%{name}.so
%{_includedir}/json
%{_libdir}/cmake/*
%{_libdir}/pkgconfig/%{name}.pc

%files help
%doc %{_docdir}/%{name}
%doc %{_docdir}/%{name}/README.md


%changelog
* Fri Jul 31 2020 wenzhanli<wenzhanli2@huawei.com> - 1.9.3-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:update version 1.9.3

* Mon Nov 25 2019 zhujunhao <zhujunhao5@huawei.com> - 1.8.4-6
- Package init
